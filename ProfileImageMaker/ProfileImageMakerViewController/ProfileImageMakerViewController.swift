//
//  ProfileImageMakerViewController.swift
//  ProfileImageMaker
//
//  Created by Won Tai Ki on 5/17/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import AVFoundation

class ProfileImageMakerViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    var captureSession: AVCaptureSession?
    
    var frontDevice : AVCaptureDevice?
    var backDevice : AVCaptureDevice?
    
    var deviceInput : AVCaptureDeviceInput?
    var videoOutput : AVCaptureVideoDataOutput = AVCaptureVideoDataOutput();
    
    @IBOutlet var previewImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.grantCameraPermission()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func grantCameraPermission() {
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { (granted : Bool) in
            if (granted) {
                self.initCameraOverlay();
                if let session = self.captureSession {
                    session.startRunning();
                }
                else {
                    
                }
            }
            else {
                let alert = UIAlertController(title: "Permission Error", message: "You should give me a permission for camera to use this app.", preferredStyle: .alert);
                
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil);
                alert.addAction(okAction);
                
                self.present(alert, animated: true, completion: nil);
            }
        }
    }
    
    func initCameraOverlay() {
        captureSession = AVCaptureSession()
        guard let session = captureSession else {
            return
        }
        captureSession = session;
        
        session.sessionPreset = AVCaptureSessionPreset1280x720;
        
//        imageViewPoo.translatesAutoresizingMaskIntoConstraints = false;
        
        // Add Camera Input
        let descoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [.builtInWideAngleCamera , .builtInDualCamera], mediaType: AVMediaTypeVideo, position: .unspecified);
        if let devices = descoverySession?.devices {
            for device in devices {
                if (device.position == .back) {
                    self.backDevice = device;
                    do {
                        deviceInput = try AVCaptureDeviceInput(device: device)
                        if session.canAddInput(deviceInput) {
                            session.addInput(deviceInput)
                        }
                    }
                    catch {
                        
                    }
                }
                if (device.position == .front) {
                    // Do something
                    self.frontDevice = device;
                }
            }
        }
        
        //previewView.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "sample buffer delegate", attributes: []))
        if session.canAddOutput(videoOutput) {
            session.addOutput(videoOutput)
        }
        
        
        
    }

    // MARK: AVCaptureVideoOutput
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        DispatchQueue.global().async {
            let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
            let cameraImage = CIImage(cvPixelBuffer: pixelBuffer!)
            // TODO: set Orientation for Input Device
            let orientCIImage = cameraImage.applyingOrientation(6);
            
            let image = UIImage(ciImage: orientCIImage)
            
            DispatchQueue.main.async { [unowned self] in
                self.previewImageView.image = image;
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
